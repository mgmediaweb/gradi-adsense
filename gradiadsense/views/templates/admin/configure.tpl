{*
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="panel">
	<div class="row moduleconfig-header">
		<div class="col text-center">
			<img src="{$module_dir|escape:'html':'UTF-8'}views/img/logo.jpg" />
		</div>
	</div>

	<hr />

	<div class="moduleconfig-content">
<form method="post" enctype="multipart/form-data">	
		<div class="row">
			<div class="col-12 col-sm-6">
				<div class="form-group">
					<label for="GA_VISIBLE">Visible</label>
					<select name="GA_VISIBLE" class="custom-select">
						<option value="1" {if $GA_VISIBLE == "1"} selected {/if}>Visible</option>
						<option value="0" {if $GA_VISIBLE == "0"} selected {/if}>Oculto</option>
					</select>
				</div>				
			</div>
			<div class="col-12 col-sm-6">
				<div class="form-group">
					<label for="GA_TITLE">Título publicidad</label>
					<input name="GA_TITLE" type="text" placeholder="Promoción Halloween" value="{$GA_TITLE}" class="form-control" required>
				</div>				
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-sm-6">
				<div class="form-group">
					<label for="GA_LABEL">Etiqueta</label>
					<input name="GA_LABEL" type="text" value="{$GA_LABEL}" class="form-control">
				</div>
			</div>
			<div class="col-12 col-sm-6">
				<div class="form-group">
					<label for="GA_URL">URL</label>
					<input name="GA_URL" type="text" placeholder="http://dominio.com" value="{$GA_URL}" class="form-control">	
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-md-4">
				{if isset($GA_IMAGE_UPLOAD) && ($GA_IMAGE_UPLOAD != "")}
					<img src="{$base_dir}upload/{$GA_IMAGE_UPLOAD}" alt="http://{$smarty.server.HTTP_HOST}/upload/{$GA_IMAGE_UPLOAD}" class="img-thumbnail" />
				{else}
					<img src="http://{$smarty.server.HTTP_HOST}{$module_dir}views/img/demo.jpg" alt="" class="img-thumbnail" />
				{/if}
			</div>
		</div>		
		<div class="row">
			<div class="col">
				<div class="form-group">
					<label for="GA_IMAGE_UPLOAD">Publicar imágen</label>
					<input name="GA_IMAGE_UPLOAD" type="file" class="form-control-file">
				</div>
			</div>
		</div>		
		<div class="row">
			<div class="col">
				<div class="form-group">
					<label for="GA_DESCRIPTION">Descripción</label>
					<textarea name="GA_DESCRIPTION" class="form-control" rows="2">{$GA_DESCRIPTION}</textarea>
				</div>
				<div class="form-group">
					<button type="submit" name="submitGradiadsenseModule" class="btn btn-primary">Guardar</button>	
				</div>
			</div>
		</div>
</form>						
		<div class="row">
			<div class="col">
				<p class="text-center">
					<strong>
						Desarrollado por 
						<a href="https://www.facebook.com/mgmediaweb" target="_blank" title="Lorem ipsum dolor">
							{l s='Gonzalo Medina' mod='gradiadsense' }
						</a>
					</strong>
				</p>
			</div>
		</div>		
	</div>
</div>