<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

class Gradiadsense extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'gradiadsense';
        $this->tab = 'main';
        $this->version = '1.0.0';
        $this->author = 'Gonzalo Medina';
        $this->need_instance = 0;
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Gradi Adsense');
        $this->description = $this->l('Módulo de mantención de publicidad');

        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        Configuration::updateValue('GA_VISIBLE','1');

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayHome');
    }   

    public function uninstall()
    {
        Configuration::deleteByName('GA_VISIBLE');
        Configuration::deleteByName('GA_TITLE');
        Configuration::deleteByName('GA_URL');
        Configuration::deleteByName('GA_LABEL');
        Configuration::deleteByName('GA_IMAGE_UPLOAD');
        Configuration::deleteByName('GA_DESCRIPTION');
        return parent::uninstall();
    }
/*
    public function hookActionProductUpdate($params) {
        $id_product = $params['id_product'];
    }    
*/
    public function getContent()
    {
        if (((bool)Tools::isSubmit('submitGradiadsenseModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign(
            array(
                'module_dir' => $this->_path,
                'base_dir' => __PS_BASE_URI__,
                'GA_VISIBLE' => Configuration::get('GA_VISIBLE'),
                'GA_TITLE' => Configuration::get('GA_TITLE'),
                'GA_URL' => Configuration::get('GA_URL'),
                'GA_LABEL' => Configuration::get('GA_LABEL'),
                'GA_IMAGE_UPLOAD' => Configuration::get('GA_IMAGE_UPLOAD'),
                'GA_DESCRIPTION' => Configuration::get('GA_DESCRIPTION')
            )            
        );

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');
        return $output;
    }

    protected function getConfigFormValues()
    {
        return array(
            'GA_VISIBLE' => Configuration::get('GA_VISIBLE'),
            'GA_TITLE' => Configuration::get('GA_TITLE'),
            'GA_URL' => Configuration::get('GA_URL'),
            'GA_LABEL' => Configuration::get('GA_LABEL'),
            'GA_IMAGE_UPLOAD' => Configuration::get('GA_IMAGE_UPLOAD'),            
            'GA_DESCRIPTION' => Configuration::get('GA_DESCRIPTION'),
        );
    }

    protected function postProcess()
    {
        if ($_FILES['GA_IMAGE_UPLOAD']["tmp_name"] != "") {
            $target_dir = _PS_UPLOAD_DIR_;
            $filetemp = $_FILES['GA_IMAGE_UPLOAD']['name'];
            $ext = pathinfo($filetemp, PATHINFO_EXTENSION);
            $file_name = rand(10000000,99999999) . ".jpg";
			$target_file = $target_dir . $file_name;

            if (move_uploaded_file($_FILES['GA_IMAGE_UPLOAD']["tmp_name"], $target_file)) {
                $file_location = basename($_FILES['GA_IMAGE_UPLOAD']["name"]);
            }
        }

        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            if($key == "GA_IMAGE_UPLOAD") {
                $getData = $file_name;
            } else {
                $getData = Tools::getValue($key);
            }
            Configuration::updateValue($key, $getData);
        }
    }

    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    public function hookDisplayHome()
    {
        $file = Configuration::get('GA_IMAGE_UPLOAD');
        if(isset($file) && ($file != "")) {
            $file = __PS_BASE_URI__ . 'upload/'.$file;
        } else {
            $file = $this->_path.'views/img/demo.jpg';
        }
        
        $this->context->smarty->assign(
            array(
                'image_url' => $file,
                'image_link' => Configuration::get('GA_URL'),
                'image_title' => Configuration::get('GA_TITLE')
            )
        );

        $visible = Configuration::get('GA_VISIBLE');

        if($visible == "1") {
            return $this->display(__FILE__,'gradiadsense.tpl');
        }
    }
}
